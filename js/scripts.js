var vrbutton = $(".vr");
var figure = $('.virtualboard');

function activateFigure() {
	figure.toggleClass('active');
}

vrbutton.on('click', function(){ activateFigure() });
figure.on('click', function(){ activateFigure() });